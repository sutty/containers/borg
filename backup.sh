#!/bin/sh
set -e

# Password can't be empty
test -n "${BORG_PASSPHRASE}"
test -n "${DEST}"
test -n "${SSH_ALIAS}"

DATE="$(date +%F)"
ORIG=/srv/data

if ! grep -q "${SSH_ALIAS}" /root/.ssh/config ; then
  echo "Host ${SSH_ALIAS}"                      >> /root/.ssh/config
  echo "  Port ${SSH_PORT:-22}"                 >> /root/.ssh/config
  echo "  HostName ${SSH_SERVER:-${SSH_ALIAS}}" >> /root/.ssh/config
  echo "  User ${SSH_USER:-root}"               >> /root/.ssh/config
  echo "  IdentityFile ${ORIG}/id_ed25519"      >> /root/.ssh/config
fi

ssh -fN ${SSH_ALIAS}

for signal in TERM QUIT HUP EXIT INT KILL; do
  trap "ssh -O exit \"${SSH_ALIAS}\"" ${signal}
done

# It'll fail the second time
borg init --encryption=repokey "${SSH_ALIAS}:${DEST}" || :
borg create -v --stats "${SSH_ALIAS}:${DEST}::${DATE}" "${ORIG}"
borg prune -v --stats --keep-daily ${KEEP} "${SSH_ALIAS}:${DEST}"

exit $?
