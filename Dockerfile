FROM sutty/monit:latest
MAINTAINER "f <f@sutty.nl>"

ENV BORG_HOST_ID borg
ENV BORG_PASSPHRASE ""
ENV KEEP 30
ENV SSH_ALIAS ""
ENV SSH_SERVER ""
ENV SSH_USER root
ENV SSH_PORT 22

RUN apk add --no-cache borgbackup openssh-client

COPY ./monit.conf /etc/monit.d/borg.conf
COPY ./backup.sh /usr/local/bin/backup
COPY ./ssh_config /root/.ssh/config
RUN chmod 755 /usr/local/bin/backup
RUN chmod -R u=rX,g=,o= /root/.ssh

VOLUME /srv/data
VOLUME /srv/backups
